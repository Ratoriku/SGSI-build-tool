#!/bin/bash

LOCALDIR=`cd "$( dirname $0 )" && pwd`
cd $LOCALDIR

rm -rf $1/data-app/*

rm -rf $1/app/cit
rm -rf $1/app/NextPay
rm -rf $1/app/MSA
rm -rf $1/app/MiuiCompass
rm -rf $1/app/XiaomiServiceFramework
rm -rf $1/app/MiuiVideoGlobal
rm -rf $1/app/Stk
rm -rf $1/app/mab
rm -rf $1/app/Youpin
rm -rf $1/app/TSMClient
rm -rf $1/app/PaymentService
rm -rf $1/app/AnalyticsCore
rm -rf $1/app/BasicDreams
rm -rf $1/app/BookmarkProvider
rm -rf $1/app/BackupAndRestore
rm -rf $1/app/MiuiBugReport
rm -rf $1/app/CatchLog
rm -rf $1/app/Mipay
rm -rf $1/app/HybridAccessory
rm -rf $1/app/HybridPlatform
rm -rf $1/app/Joyose
rm -rf $1/app/MiLinkService
rm -rf $1/app/KSICibaEngine
rm -rf $1/app/MiuiDaemon
rm -rf $1/app/BuiltInPrintService
rm -rf $1/app/PrintSpooler
rm -rf $1/priv-app/Backup
rm -rf $1/priv-app/Music
rm -rf $1/priv-app/CleanMaster
rm -rf $1/priv-app/MiuiVideo
rm -rf $1/priv-app/MiRecycle
rm -rf $1/priv-app/MiWebView
rm -rf $1/priv-app/MiService
rm -rf $1/priv-app/Velvet
rm -rf $1/priv-app/BackupAndRestore
